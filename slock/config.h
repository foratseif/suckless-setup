#ifndef MESSAGE_LEN
#define MESSAGE_LEN 50
#endif

#ifndef USER_LEN
#define USER_LEN 20
#endif

/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";


static const char *colorname[NUMCOLS] = {
	//[INIT] =   "darkblue",     /* after initialization */
	//[INPUT] =  "#000000",   /* during input */
	//[FAILED] = "#CC3333",   /* wrong password */
	[INIT] =   "#000000",     /* after initialization */
	[INPUT] =  "#000000",   /* during input */
	[FAILED] = "#000000",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

/* default message */
char default_message[MESSAGE_LEN] = "Computer is locked. - User: ";
char message[MESSAGE_LEN] = "";
char username[USER_LEN] = "unknown";

/* text color */
static const char * text_color = "#ffffff";

/* text size (must be a valid size) */
static const char *font = "-xos4-terminus-medium-r-normal--16-160-72-72-c-80-iso10646-1";
